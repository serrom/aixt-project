// This file is part of the Aixt project, https://gitlab.com/fermarsan/aixt-project.git
//
// The MIT License (MIT)
// 
// Copyright (c) 2022 Fernando Martínez Santa

#ifndef _SLEEP_MS_H_
#define _SLEEP_MS_H_

#define sleep_ms(TIME)    __delay_ms(TIME)

#endif  //_SLEEP_MS_H_