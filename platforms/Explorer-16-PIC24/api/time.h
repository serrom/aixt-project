// This file is part of the Aixt project, https://gitlab.com/fermarsan/aixt-project.git
//
// The MIT License (MIT)
// 
// Copyright (c) 2022 Fernando Martínez Santa

#ifndef _TIME_H_
#define _TIME_H_

#include "./time/sleep_us.h"
#include "./time/sleep_ms.h"

#endif  // _TIME_H_